#!/bin/bash
# Requirements:
# - xclip or wl-paste
# - notify-send
# - rofi
# - kdeconnect-cli

# If any arguments were passed, treat them as filenames to send to the other device
files=( "$@" )

# If no arguments passed, try to get a HTTP(S) URL from clipboard
if (( ${#files[@]} == 0 )); then
    files=$(xclip -o -selection clipboard) # replace with `wl-paste` on wayland
    grep -P 'https?://' <<< "${files}" || notify-send -u critical -t 10000 "Clipboard does not contain a valid http(s) URL!" && exit 1
fi

# Spawn a rofi menu to choose the kdeconnect device
device_id=$(kdeconnect-cli -a --id-name-only 2>&1 | rofi -dmenu -p '> ' -mesg "Choose Device")
if [[ -z "${device_id}" ]]; then
    exit
else
    device_id=$(awk '{ print $1 }' <<< "${device_id}")
fi

# Send each filename from the list to chosen device
for filename in "${files[@]}"; do
    notify-send -t 5000 "Sending via kdeconnect..." "$(kdeconnect-cli -d "${device_id}" --share "${filename}")"
done
