#!/bin/bash

title="Select an option: "
widthpercent=33

typeset -A menu
menu=(
    [1. Lock Screen  ]="light-locker-command -l"
    [2. Sleep        ]="systemctl suspend"
    [3. Reboot       ]="systemctl reboot"
    [4. Shutdown     ]="systemctl poweroff"
    [5. Hybrid Sleep ]="systemctl hybrid-sleep"
    [6. Hibernate    ]="systemctl hibernate"
)
