/- kdl-version 1
// This config is in the KDL format: https://kdl.dev
// "/-" comments out the following node.
// Check the wiki for a full description of the configuration:
// https://github.com/YaLTeR/niri/wiki/Configuration:-Overview

cursor {
    xcursor-theme "breeze_cursors"
    xcursor-size 24
}

// Input device configuration.
// Find more information on the wiki:
// https://github.com/YaLTeR/niri/wiki/Configuration:-Input
input {
    keyboard {
        xkb {
            // You can set rules, model, layout, variant and options.
            // For more information, see xkeyboard-config(7).
            layout "pl"
            options "caps:swapescape"
        }
    }

    touchpad {
        tap // tap to click
    }

    tablet {}

    // warp-mouse-to-focus conflicts with the "scroll to change window" behavior
    // which I configured for Waybar
}

// You can configure outputs by their name, which you can find
// by running `niri msg outputs` while inside a niri instance.
// The built-in laptop monitor is usually called "eDP-1".
// Find more information on the wiki:
// https://github.com/YaLTeR/niri/wiki/Configuration:-Outputs
/-output "eDP-1" {}

hotkey-overlay {
    skip-at-startup
}

// Settings that influence how windows are positioned and sized.
// Find more information on the wiki:
// https://github.com/YaLTeR/niri/wiki/Configuration:-Layout
layout {
    // Set gaps around windows in logical pixels.
    gaps 16

    // When to center a column when changing focus.
    center-focused-column "never"

    // Widths that "switch-preset-column-width" (Mod+R) toggles between.
    preset-column-widths {
        // Proportion sets the width as a fraction of the output width, taking gaps into account.
        proportion 0.33333
        proportion 0.5
        proportion 0.66667
    }
    // Default width of the new windows.
    default-column-width { proportion 0.5; }

    // You can change how the focus ring looks.
    focus-ring {
        // How many logical pixels the ring extends out from the windows.
        width 5

        // Color of the ring on the active monitor.
        active-color "#5dc76c" // Matches Yaru Green GTK Theme
        // Color of the ring on inactive monitors.
        inactive-color "#109b26"
    }
}

// You can change the path where screenshots are saved.
// A ~ at the front will be expanded to the home directory.
// The path is formatted with strftime(3) to give you the screenshot date and time.
screenshot-path "~/Pictures/Screenshots/Screenshot %Y-%m-%d %H:%M:%S.png"

// Animation settings.
// The wiki explains how to configure individual animations:
// https://github.com/YaLTeR/niri/wiki/Configuration:-Animations
animations {
    // Slow down all animations by this factor. Values below 1 speed them up instead.
    // slowdown 3.0
}

/// WINDOW RULES
// Window rules let you adjust behavior for individual windows.
// Find more information on the wiki:
// https://github.com/YaLTeR/niri/wiki/Configuration:-Window-Rules

// Work around WezTerm's initial configure bug
// by setting an empty default-column-width.
window-rule {
    match app-id=r#"^org\.wezfurlong\.wezterm$"#
    default-column-width {}
}

// Password prompts
window-rule {
    match app-id=r#"^polkit-gnome-authentication-agent-1$"#
    match app-id=r#"^gcr-prompter$"#
    block-out-from "screen-capture"
    // TODO: make floating when possible
}

// Block out password managers and such from screen capture.
window-rule {
    match app-id=r#"^org\.keepassxc\.KeePassXC$"#
    match app-id=r#"^org\.gnome\.World\.Secrets$"#
    match app-id=r#"firefox$"# title=r#"Bitwarden — Mozilla Firefox$"#

    // Visible only on built-in region select screenshots
    // Blocked from built-in window/screen screenshots
    // Blocked from all third party screen capture tools
    block-out-from "screen-capture"

    // Use this instead if you want them visible on third-party screenshot tools.
    // block-out-from "screencast"
}

// Default Column Width
window-rule {
    match app-id=r#"^Alacritty$"#
    match app-id=r#"^thunderbird$"#
    match app-id=r#"^Spotify$"#

    default-column-width { proportion 0.66667; }
}
window-rule {
    match app-id=r#"^firefox$"#
    match app-id=r#"^org\.mozilla\.firefox$"#
    match app-id=r#"^potion craft\.exe$"#

    default-column-width { proportion 1.0; }
}

// KDE Connect Presentation Pointer is broken
// and needs upstream to use layer-shell instead of a normal window

// Block out mako notifications from screencasts.
layer-rule {
    match namespace="^notifications$"

    block-out-from "screencast"
}

// Uncomment to preview what windows would be hidden from screen capture
/-debug {
    preview-render "screen-capture"
    //preview-render "screencast"
}

binds {
    // Keys consist of modifiers separated by + signs, followed by an XKB key name in the end. 
    // To find an XKB name for a particular key, you may use a program like wev.
    //
    // "Mod" is equal to Super when running on a TTY, and to Alt when running as a winit window.

    // Mod-Shift-/, which is usually the same as Mod-?,
    // shows a list of important hotkeys.
    Mod+Shift+Slash { show-hotkey-overlay; }

    // Running programs: terminal, app launcher, screen locker.
    Mod+T { spawn "alacritty"; }
    Mod+D { spawn "fuzzel"; }
    Super+Alt+L { spawn "loginctl lock-session"; }

    // Volume keys mappings for PipeWire & WirePlumber.
    // The allow-when-locked=true property makes them work even when the session is locked.
    XF86AudioRaiseVolume allow-when-locked=true { spawn "wpctl" "set-volume" "@DEFAULT_AUDIO_SINK@" "0.05+"; }
    XF86AudioLowerVolume allow-when-locked=true { spawn "wpctl" "set-volume" "@DEFAULT_AUDIO_SINK@" "0.05-"; }
    XF86AudioMute        allow-when-locked=true { spawn "wpctl" "set-mute" "@DEFAULT_AUDIO_SINK@" "toggle"; }
    XF86AudioMicMute     allow-when-locked=true { spawn "wpctl" "set-mute" "@DEFAULT_AUDIO_SOURCE@" "toggle"; }

    XF86MonBrightnessUp   allow-when-locked=true { spawn "/usr/bin/fish" "-c" r#"pkexec xfpm-power-backlight-helper --set-brightness (math min \( \( max (xfpm-power-backlight-helper --get-brightness) + 32, 0 \), 255 \))"#; }
    XF86MonBrightnessDown allow-when-locked=true { spawn "/usr/bin/fish" "-c" r#"pkexec xfpm-power-backlight-helper --set-brightness (math min \( \( max (xfpm-power-backlight-helper --get-brightness) - 32, 0 \), 255 \))"#; }

    // TODO: there is no obvious way to toggle the touchpad
    //XF86TouchpadToggle   allow-when-locked=true { spawn ""}

    // TODO: add wleave config to the config repo
    XF86Sleep { spawn "wleave" "--show-keybinds"; }

    Mod+Q { close-window; }

    // Mod+Dir - move focus
    Mod+Left  { focus-column-left; }
    Mod+Down  { focus-window-down; }
    Mod+Up    { focus-window-up; }
    Mod+Right { focus-column-right; }
    Mod+H     { focus-column-left; }
    Mod+J     { focus-window-down; }
    Mod+K     { focus-window-up; }
    Mod+L     { focus-column-right; }

    // Mod+Ctrl+Dir - move column/window
    Mod+Ctrl+Left  { move-column-left; }
    Mod+Ctrl+Down  { move-window-down; }
    Mod+Ctrl+Up    { move-window-up; }
    Mod+Ctrl+Right { move-column-right; }
    Mod+Ctrl+H     { move-column-left; }
    Mod+Ctrl+J     { move-window-down; }
    Mod+Ctrl+K     { move-window-up; }
    Mod+Ctrl+L     { move-column-right; }

    // Alternative commands that move across workspaces when reaching
    // the first or last window in a column.
    // Mod+J     { focus-window-or-workspace-down; }
    // Mod+K     { focus-window-or-workspace-up; }
    // Mod+Ctrl+J     { move-window-down-or-to-workspace-down; }
    // Mod+Ctrl+K     { move-window-up-or-to-workspace-up; }

    Mod+Home { focus-column-first; }
    Mod+End  { focus-column-last; }
    Mod+Ctrl+Home { move-column-to-first; }
    Mod+Ctrl+End  { move-column-to-last; }

    // Mod+Shift+Dir - change focused screen
    Mod+Shift+Left  { focus-monitor-left; }
    Mod+Shift+Down  { focus-monitor-down; }
    Mod+Shift+Up    { focus-monitor-up; }
    Mod+Shift+Right { focus-monitor-right; }
    Mod+Shift+H     { focus-monitor-left; }
    Mod+Shift+J     { focus-monitor-down; }
    Mod+Shift+K     { focus-monitor-up; }
    Mod+Shift+L     { focus-monitor-right; }

    // Mod+Shift+Ctrl+Dir - move column across screens
    Mod+Shift+Ctrl+Left  { move-column-to-monitor-left; }
    Mod+Shift+Ctrl+Down  { move-column-to-monitor-down; }
    Mod+Shift+Ctrl+Up    { move-column-to-monitor-up; }
    Mod+Shift+Ctrl+Right { move-column-to-monitor-right; }
    Mod+Shift+Ctrl+H     { move-column-to-monitor-left; }
    Mod+Shift+Ctrl+J     { move-column-to-monitor-down; }
    Mod+Shift+Ctrl+K     { move-column-to-monitor-up; }
    Mod+Shift+Ctrl+L     { move-column-to-monitor-right; }

    // Alternatively, there are commands to move just a single window:
    // Mod+Shift+Ctrl+Left  { move-window-to-monitor-left; }
    // ...

    // And you can also move a whole workspace to another monitor:
    // Mod+Shift+Ctrl+Left  { move-workspace-to-monitor-left; }
    // ...

    Mod+Page_Down      { focus-workspace-down; }
    Mod+Page_Up        { focus-workspace-up; }
    Mod+U              { focus-workspace-down; }
    Mod+I              { focus-workspace-up; }
    Mod+Ctrl+Page_Down { move-column-to-workspace-down; }
    Mod+Ctrl+Page_Up   { move-column-to-workspace-up; }
    Mod+Ctrl+U         { move-column-to-workspace-down; }
    Mod+Ctrl+I         { move-column-to-workspace-up; }

    // Alternatively, there are commands to move just a single window:
    // Mod+Ctrl+Page_Down { move-window-to-workspace-down; }
    // ...

    Mod+Shift+Page_Down { move-workspace-down; }
    Mod+Shift+Page_Up   { move-workspace-up; }
    Mod+Shift+U         { move-workspace-down; }
    Mod+Shift+I         { move-workspace-up; }

    // You can bind mouse wheel scroll ticks using the following syntax.
    // These binds will change direction based on the natural-scroll setting.
    //
    // To avoid scrolling through workspaces really fast, you can use
    // the cooldown-ms property. The bind will be rate-limited to this value.
    // You can set a cooldown on any bind, but it's most useful for the wheel.
    Mod+WheelScrollDown      cooldown-ms=150 { focus-workspace-down; }
    Mod+WheelScrollUp        cooldown-ms=150 { focus-workspace-up; }
    Mod+Ctrl+WheelScrollDown cooldown-ms=150 { move-column-to-workspace-down; }
    Mod+Ctrl+WheelScrollUp   cooldown-ms=150 { move-column-to-workspace-up; }

    Mod+WheelScrollRight      { focus-column-right; }
    Mod+WheelScrollLeft       { focus-column-left; }
    Mod+Ctrl+WheelScrollRight { move-column-right; }
    Mod+Ctrl+WheelScrollLeft  { move-column-left; }

    // Usually scrolling up and down with Shift in applications results in
    // horizontal scrolling; these binds replicate that.
    Mod+Shift+WheelScrollDown      { focus-column-right; }
    Mod+Shift+WheelScrollUp        { focus-column-left; }
    Mod+Ctrl+Shift+WheelScrollDown { move-column-right; }
    Mod+Ctrl+Shift+WheelScrollUp   { move-column-left; }

    // Similarly, you can bind touchpad scroll "ticks".
    // Touchpad scrolling is continuous, so for these binds it is split into
    // discrete intervals.
    // These binds are also affected by touchpad's natural-scroll, so these
    // example binds are "inverted", since we have natural-scroll enabled for
    // touchpads by default.
    // Mod+TouchpadScrollDown { spawn "wpctl" "set-volume" "@DEFAULT_AUDIO_SINK@" "0.02+"; }
    // Mod+TouchpadScrollUp   { spawn "wpctl" "set-volume" "@DEFAULT_AUDIO_SINK@" "0.02-"; }

    // You can refer to workspaces by index. However, keep in mind that
    // niri is a dynamic workspace system, so these commands are kind of
    // "best effort". Trying to refer to a workspace index bigger than
    // the current workspace count will instead refer to the bottommost
    // (empty) workspace.
    //
    // For example, with 2 workspaces + 1 empty, indices 3, 4, 5 and so on
    // will all refer to the 3rd workspace.
    Mod+1 { focus-workspace 1; }
    Mod+2 { focus-workspace 2; }
    Mod+3 { focus-workspace 3; }
    Mod+4 { focus-workspace 4; }
    Mod+5 { focus-workspace 5; }
    Mod+6 { focus-workspace 6; }
    Mod+7 { focus-workspace 7; }
    Mod+8 { focus-workspace 8; }
    Mod+9 { focus-workspace 9; }
    Mod+Ctrl+1 { move-column-to-workspace 1; }
    Mod+Ctrl+2 { move-column-to-workspace 2; }
    Mod+Ctrl+3 { move-column-to-workspace 3; }
    Mod+Ctrl+4 { move-column-to-workspace 4; }
    Mod+Ctrl+5 { move-column-to-workspace 5; }
    Mod+Ctrl+6 { move-column-to-workspace 6; }
    Mod+Ctrl+7 { move-column-to-workspace 7; }
    Mod+Ctrl+8 { move-column-to-workspace 8; }
    Mod+Ctrl+9 { move-column-to-workspace 9; }

    // Alternatively, there are commands to move just a single window:
    // Mod+Ctrl+1 { move-window-to-workspace 1; }

    // Switches focus between the current and the previous workspace.
    // Mod+Tab { focus-workspace-previous; }

    Mod+Comma  { consume-window-into-column; }
    Mod+Period { expel-window-from-column; }

    // There are also commands that consume or expel a single window to the side.
    // Mod+BracketLeft  { consume-or-expel-window-left; }
    // Mod+BracketRight { consume-or-expel-window-right; }

    Mod+R { switch-preset-column-width; }
    Mod+Shift+R { reset-window-height; }
    Mod+F { maximize-column; }
    Mod+Shift+F { fullscreen-window; }
    Mod+Ctrl+C { center-column; }

    // Finer width adjustments.
    // This command can also:
    // * set width in pixels: "1000"
    // * adjust width in pixels: "-5" or "+5"
    // * set width as a percentage of screen width: "25%"
    // * adjust width as a percentage of screen width: "-10%" or "+10%"
    // Pixel sizes use logical, or scaled, pixels. I.e. on an output with scale 2.0,
    // set-column-width "100" will make the column occupy 200 physical screen pixels.
    Mod+Minus { set-column-width "-10%"; }
    Mod+Equal { set-column-width "+10%"; }

    // Finer height adjustments when in column with other windows.
    Mod+Shift+Minus { set-window-height "-10%"; }
    Mod+Shift+Equal { set-window-height "+10%"; }

    // Actions to switch layouts.
    // Note: if you uncomment these, make sure you do NOT have
    // a matching layout switch hotkey configured in xkb options above.
    // Having both at once on the same hotkey will break the switching,
    // since it will switch twice upon pressing the hotkey (once by xkb, once by niri).
    // Mod+Space       { switch-layout "next"; }
    // Mod+Shift+Space { switch-layout "prev"; }

    // Screenshot binds
    Print { screenshot-screen; }
    Shift+Print { screenshot; }
    Ctrl+Print { screenshot-window; }
    // Alt screenshot binds
    Mod+P { screenshot-screen; }
    Mod+Shift+P { screenshot; }
    Mod+Ctrl+P { screenshot-window; }

    // The quit action will show a confirmation dialog to avoid accidental exits.
    //Mod+Ctrl+Q { quit; }
    Mod+Ctrl+Q { spawn "wleave" "--show-keybinds"; }

    // Powers off the monitors. To turn them back on, do any input like
    // moving the mouse or pressing any other key.
    Mod+Shift+O { power-off-monitors; }

    // Clipboard manager
    Mod+C { spawn "bash" "-c" "cliphist list | fuzzel --dmenu --prompt 'Copy to clipboard:' | cliphist decode | wl-copy"; }
    Mod+Shift+C { spawn "bash" "-c" "cliphist list | fuzzel --dmenu --prompt 'Delete entry:' | cliphist delete"; }
}

// Environment variables for processes spawned by niri
// [global variables in ~/.config/environment.d/*.conf]
environment {
    XDG_CURRENT_DESKTOP "niri"
    QT_QPA_PLATFORM "wayland"
    DISPLAY ":12"
}

/// AUTOSTART
// Note that running niri as a session supports xdg-desktop-autostart,
// which may be more convenient to use.

/// Background Stuff
// Clipboard manager
spawn-at-startup "wl-paste" "--watch" "cliphist" "store"
// Desktop background
spawn-at-startup "fish" "-c" "swaybg --color '#333333' -i ~/.config/niri/demonic-xenia.png -m center &; disown"
// Taskbar
spawn-at-startup "waybar"
// Polkit authentication agent
spawn-at-startup "/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1"
// Display profiles
spawn-at-startup "kanshi"
// XWayland
spawn-at-startup "bash" "-c" "xwayland-satellite :12 2>&1 > ~/.var/xwayland-satellite.log"
// Screen locker integration
spawn-at-startup "bash" "-c" "~/.config/niri/dbus_gtklock.py 2>&1 > ~/.var/dbus_gtklock.log"

/// Tray Icons
// Battery tray icon and level notifications
spawn-at-startup "cbatticon" "-l" "30" "-r" "15"
// Network Manager tray icon
spawn-at-startup "nm-applet"
// Blueman tray icon
spawn-at-startup "blueman-applet"
// Power manager
spawn-at-startup "xfce4-power-manager"

// Idk what this does exactly but it's necessary
spawn-at-startup "dbus-update-activation-environment"
