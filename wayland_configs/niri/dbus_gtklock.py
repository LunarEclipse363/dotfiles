#!/usr/bin/env python3
import logging, os, shutil, subprocess
from typing import Optional

import dbus
from dbus.mainloop.glib import DBusGMainLoop
from gi.repository import GLib # type: ignore

logger = logging.getLogger()

DBusGMainLoop(set_as_default=True)
system_bus = dbus.SystemBus()

# Docs: https://www.freedesktop.org/software/systemd/man/latest/org.freedesktop.login1.html
LOGIND_BUS_NAME = "org.freedesktop.login1"

logind = system_bus.get_object(bus_name=LOGIND_BUS_NAME, object_path="/org/freedesktop/login1")

match (shutil.which("gtklock")):
    case None:
        logging.error("Couldn't find the screen locker executable!")
        exit(1)
    case path:
        SCREEN_LOCKER = path

screen_locker_handle: Optional[subprocess.Popen] = None
inhibitor_lock_fd = None


def on_lock_signal():
    global screen_locker_handle
    if screen_locker_handle is not None and screen_locker_handle.poll() is not None:
        # this means the process exited
        screen_locker_handle = None
        logging.debug("Cleaned up old screen_locker_handle.")

    if screen_locker_handle is not None:
        logging.info("Received a lock signal but screen is already locked.")
        return

    logging.info("Locking the screen...")
    screen_locker_handle = subprocess.Popen([SCREEN_LOCKER])


def on_unlock_signal():
    global screen_locker_handle
    if screen_locker_handle is not None and screen_locker_handle.poll() is not None:
        # this means the process exited
        screen_locker_handle = None
        logging.debug("Cleaned up old screen_locker_handle.")

    if screen_locker_handle is None:
        logging.info("Received an unlock signal but screen is already unlocked.")
        return

    logging.info("Unlocking the screen...")
    screen_locker_handle.terminate()
    try:
        screen_locker_handle.wait(timeout=30)
    except subprocess.TimeoutExpired as _:
        logging.warning("Timed out waiting for the screen locker to terminate!")
    else:
        screen_locker_handle = None
        logging.debug("Cleaned up screen_locker_handle.")

def on_prepare_for_sleep_signal(before_sleep: bool):
    global inhibitor_lock_fd
    if before_sleep:
        logging.info("System going to sleep, locking the screen...")
        on_lock_signal()
        logging.debug("Releasing inhibitor_lock_fd...")
        del inhibitor_lock_fd
        inhibitor_lock_fd = None
    else:
        logging.debug("Woke up from sleep, acquiring inhibitor_lock_fd...")
        inhibitor_lock_fd = logind.get_dbus_method("Inhibit", dbus_interface="org.freedesktop.login1.Manager")("sleep", "dbus_gtklock.py", "locking the screen before sleep", "delay")


def main():
    logging.basicConfig(format="[{levelname}] {asctime}: {message}", datefmt="%Y-%m-%dT%H:%M:%S", style='{', level=logging.DEBUG)

    logind_session_path = logind.get_dbus_method("GetSession", dbus_interface="org.freedesktop.login1.Manager")(os.getenv("XDG_SESSION_ID"))

    system_bus.add_signal_receiver(on_lock_signal, signal_name="Lock", dbus_interface="org.freedesktop.login1.Session", bus_name=LOGIND_BUS_NAME, path=logind_session_path)
    system_bus.add_signal_receiver(on_unlock_signal, signal_name="Unlock", dbus_interface="org.freedesktop.login1.Session", bus_name=LOGIND_BUS_NAME, path=logind_session_path)

    global inhibitor_lock_fd
    inhibitor_lock_fd = logind.get_dbus_method("Inhibit", dbus_interface="org.freedesktop.login1.Manager")("sleep", "dbus_gtklock.py", "locking the screen before sleep", "delay")
    system_bus.add_signal_receiver(on_prepare_for_sleep_signal, signal_name="PrepareForSleep", dbus_interface="org.freedesktop.login1.Manager", bus_name=LOGIND_BUS_NAME, path="/org/freedesktop/login1")

    logging.info(f"Connected to systemd_logind session at: {logind_session_path}")

    GLib.MainLoop().run()


if __name__ == "__main__":
    main()
