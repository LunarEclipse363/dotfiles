# My color theme for the os

## main scheme (WCAG AAA compliant)
foreground-active = ffc4ff
foreground-inactive = c196d1 # Only use on normal-text with background
background = 3d163d
background-highlighted = 522d5d
accent = ff5bb5 # Don't use with text at all

## terminal colors (WCAG AA compliant if black or main.background is used as the background)
black = 3D313D
gray-dark = 9B879B
red = FF4760
red-bright = FF7777
green = 4ACC3F
green-bright = 9AFF92
yellow = F5D400
yellow-bright = FEFF92
blue = 5A86FF
blue-bright = 8FADFF
magenta = DD4BE7
magenta-bright = EB96F1
cyan = 1ABEFF
cyan-bright = A4E5FF
gray-light = BDAEC6
white = FFFFFF

```Xresources
! special
*.foreground:   #ffc4ff
*.background:   #3d163d
*.cursorColor:  #ffc4ff

! black
*.color0:       #3d313d
*.color8:       #9b879b

! red
*.color1:       #ff4760
*.color9:       #ff7777

! green
*.color2:       #4acc3f
*.color10:      #9aff92

! yellow
*.color3:       #f5d400
*.color11:      #feff92

! blue
*.color4:       #5a86ff
*.color12:      #8fadff

! magenta
*.color5:       #dd4be7
*.color13:      #eb96f1

! cyan
*.color6:       #1abeff
*.color14:      #a4e5ff

! white
*.color7:       #bdaec6
*.color15:      #ffffff
```
