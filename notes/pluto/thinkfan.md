# Various thinkfan configs for reference
## Some bbs.archlinux.org user's x220 config
*doesn't have 6th fan level utilized at all, overall a dumb config*
```
(0,     0,      55)
(1,     55,     61)
(2,     57,     65)
(3,     61,     69)
(4,     69,     81)
(5,     79,     86)
(7,     84,     32767)
```

## Some reddit user's x201 config
*utilizes all levels and only falls back after the load is definitely done*
```
(0,	0,	55)
(1,	54,	60)
(2,	55,	64)
(3,	57,	67)
(4,	56,	74)
(5,	59,	79)
(6,     64,     81)
(7,	63,	32767)
```
## My current config 
*tries to stabilize the temperatures for a given load*
```
hwmon /sys/class/hwmon/hwmon0/temp1_input
hwmon /sys/class/hwmon/hwmon4/temp1_input
hwmon /sys/class/hwmon/hwmon4/temp2_input
hwmon /sys/class/hwmon/hwmon4/temp3_input

tp_fan /proc/acpi/ibm/fan

# (lvl, t_down, t_up)
(0,     0,      58)
(1,     55,     63)
(2,     58,     68)
(3,     62,     73)
(4,     66,     75)
(5,     69,     78)
(6, 	72,	80)
(7,     69,     32767)
```
*older WIP version*
```
hwmon /sys/class/hwmon/hwmon0/temp1_input
hwmon /sys/class/hwmon/hwmon4/temp1_input
hwmon /sys/class/hwmon/hwmon4/temp2_input
hwmon /sys/class/hwmon/hwmon4/temp3_input

tp_fan /proc/acpi/ibm/fan

# this part possibly needs tweaking
(0,     0,      55)
(1,     54,     60)
(2,     55,     64)
(3,     57,     67)
(4,     56,     70)
(5,     62,     75)
# from here on the config is pretty well tweaked
(6, 	65,	78)
(7,     69,     32767)
```
