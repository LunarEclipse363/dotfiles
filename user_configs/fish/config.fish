if status is-interactive
    # Commands to run in interactive sessions can go here

    # If not running in a full graphical terminal
    if test $(tput colors) -lt 256
        # TODO: remove this since youre not using tide
        # These need to be -x or --export'ed to work
        set -gx tide_character_icon '>'
        set -gx tide_character_vi_icon_default '<'
        set -gx tide_character_vi_icon_replace 'R'
        set -gx tide_character_vi_icon_visual 'V'
    end
end


