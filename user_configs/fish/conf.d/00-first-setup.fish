if not set -q lunareclipse_config_needs_first_setup
    set -U lunareclipse_config_needs_first_setup
    set -U fish_greeting # Disable greeting
    fish_vi_key_bindings # Vim keybinds

    # Change fisher install path
    set -Ux fisher_path "$HOME/.local/share/fish/fisher"

    # Bootstrap fisher
    if not test "$(type -t fisher 2>/dev/null)" = "function"
        curl -sL https://raw.githubusercontent.com/jorgebucaran/fisher/main/functions/fisher.fish | source
        and fisher install jorgebucaran/fisher
        and fisher update
        or echo Failed to bootstrap fisher
    end

    # Prompt setup
    set -U fish_color_host bryellow
    set -U fish_color_host_remote yellow
    set -U fish_color_cwd brpurple
    set -Ue fish_color_cwd_root
end
