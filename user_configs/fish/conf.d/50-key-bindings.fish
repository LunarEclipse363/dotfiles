# An easily accessible keybind to accept autosuggestions
bind \ef accept-autosuggestion
bind --mode insert \ef accept-autosuggestion
bind \eF forward-word
bind --mode insert \eF forward-word

# Add visual_line mode
bind --preset -M visual | sed -e 's/-M visual/-M visual_line/' | source -
bind -m visual_line V beginning-of-line begin-selection end-of-line repaint-mode
bind -M visual_line j down-line end-of-line swap-selection-start-stop beginning-of-line swap-selection-start-stop
bind -M visual_line k up-line beginning-of-line swap-selection-start-stop end-of-line swap-selection-start-stop
bind -M visual_line -m default \e end-selection repaint-mode
