function remove_wine_file_associations --description 'Removes all automatically created wine file associations'
    rm -f ~/.local/share/applications/wine-extension*.desktop
    rm -f ~/.local/share/icons/hicolor/*/*/application-x-wine-extension*
    rm -f ~/.local/share/mime/packages/x-wine*
    rm -f ~/.local/share/mime/application/x-wine-extension*
    update-mime-database ~/.local/share/mime/
    update-desktop-database ~/.local/share/applications
end
