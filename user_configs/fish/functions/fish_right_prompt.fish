function fish_right_prompt
    set -l normal (set_color normal)

    echo -n -s [ (set_color brblue) (date +%H:%M) $normal ]
end
