function findfeeds --description 'Get all link[rel=alternate] elements from a website'
  set -l rawfeeds $(curl --no-progress-meter --location \
    -H "Accept: text/html" \
    $argv[1] \
  | htmlq 'head link[rel=alternate]')

  set -l bold $(tput bold)
  set -l normal $(tput sgr0)

  for feed in $rawfeeds
    set -l feed_title $(echo $feed | htmlq 'link' --attribute "title")
    set -l feed_type  $(echo $feed | htmlq 'link' --attribute "type")
    set -l feed_href  $(echo $feed | htmlq 'link' --attribute "href")

    printf "Feed: $bold%s$normal\nType: %s\nLink: %s\n\n" "$feed_title" "$feed_type" "$feed_href"
  end
end
