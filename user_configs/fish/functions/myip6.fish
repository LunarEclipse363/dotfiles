function myip6 --wraps=dig\ +short\ AAAA\ myip.opendns.com\ \'@resolver1.opendns.com\' --description alias\ myip6=dig\ +short\ AAAA\ myip.opendns.com\ \'@resolver1.opendns.com\'
  dig +short AAAA myip.opendns.com '@resolver1.opendns.com' $argv
        
end
