function autorandr-unfuck --wraps='autorandr --change' --description 'alias autorandr-unfuck autorandr --change'
  autorandr --change $argv
        
end
