function set_backlight --description 'Sets the backlight brightness of the internal screen. Usage: set_backlight <brightness 0-100>'
pkexec xfpm-power-backlight-helper --set-brightness $(math $argv[1] \* 255 / 100)
end
