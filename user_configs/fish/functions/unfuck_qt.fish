function unfuck_qt --wraps='yay -S --rebuild --noconfirm qt5-styleplugins qt6gtk2' --description 'alias unfuck_qt yay -S --rebuild --noconfirm qt5-styleplugins qt6gtk2'
  yay -S --rebuild --noconfirm qt5-styleplugins qt6gtk2 $argv
        
end
