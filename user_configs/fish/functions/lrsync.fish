function lrsync --wraps='rsync -e "ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no"' --description 'alias lrsync=rsync -e "ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no"'
  rsync -e "ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no" $argv
        
end
