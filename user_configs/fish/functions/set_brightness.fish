function set_brightness --description 'Sets the brightness of an external screen. Usage: set_brightness <brightness 0-100>'
ddcutil setvcp 10 $argv[1]
end
