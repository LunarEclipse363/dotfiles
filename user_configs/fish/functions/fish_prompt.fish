function fish_prompt --description 'My custom prompt'
    set -l last_pipestatus $pipestatus
    set -lx __fish_last_status $status # Export for __fish_print_pipestatus.
    set -l normal (set_color normal)

    set -q fish_color_status
    or set -g fish_color_status red
    set -q fish_color_suffix
    or set -g fish_color_suffix brblue
    set -q fish_color_suffix_root
    or set -g fish_color_suffix_root brred

    if test "$fish_key_bindings" = fish_vi_key_bindings
        or test "$fish_key_bindings" = fish_hybrid_key_bindings
        switch $fish_bind_mode
            case default
                set -f mode_prompt (set_color --bold brgreen)'<'
            case insert
                set -f mode_prompt (set_color --bold brblue)'>'
            case replace_one
                set -f mode_prompt (set_color --bold cyan)'R'
            case replace
                set -f mode_prompt (set_color --bold magenta)'R'
            case visual
                set -f mode_prompt (set_color --bold cyan)'V'
            case visual_line
                set -f mode_prompt (set_color --bold magenta)'V'
        end
        set -f mode_prompt $mode_prompt(set_color normal)
    end

    # Color the prompt differently when we're root
    set -l color_cwd $fish_color_cwd
    set -l suffix (set_color $fish_color_suffix)'>'
    if functions -q fish_is_root_user; and fish_is_root_user
        if set -q fish_color_cwd_root
            set color_cwd $fish_color_cwd_root
        end
        set suffix (set_color $fish_color_suffix_root)'#'
    end

    if set -q mode_prompt
        set -f suffix $mode_prompt
    end

    # Write pipestatus
    # If the status was carried over (if no command is issued or if `set` leaves the status untouched), don't bold it.
    set -l bold_flag --bold
    set -q __fish_prompt_status_generation
    or set -g __fish_prompt_status_generation $status_generation
    if test $__fish_prompt_status_generation = $status_generation
        set bold_flag
    end
    set __fish_prompt_status_generation $status_generation

    set -l status_color (set_color $fish_color_status)
    set -l statusb_color (set_color $bold_flag $fish_color_status)
    set -l prompt_status (__fish_print_pipestatus "[" "]" "|" "$status_color" "$statusb_color" $last_pipestatus)

    set -f login (prompt_login)':'
    #if test (tput cols) -lt 90
    #    set -f login
    #end

    set -f vcs (fish_vcs_prompt)$normal
    if test (tput cols) -lt 120
        set -f vcs
    end

    #echo -n -s (prompt_login)' ' (set_color $color_cwd) (prompt_pwd) $normal (fish_vcs_prompt) $normal " "$prompt_status $suffix " "
    echo -n -s [ $login (set_color $color_cwd) (prompt_pwd) $normal $vcs ] " "$prompt_status $suffix $normal " "
end
