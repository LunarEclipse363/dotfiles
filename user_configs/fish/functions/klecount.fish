function klecount --description Counts\ the\ keys\ in\ the\ \'json\'\ from\ http://www.keyboard-layout-editor.com/\ taken\ from\ the\ system\ clipboard
 echo ["$(xclip -o -selection clipboard)"] | hjson -c 2>/dev/null | jq '[.. | select(type == "string")] | length'; 
end
