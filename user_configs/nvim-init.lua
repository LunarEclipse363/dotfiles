-- Bootstrap lazy.nvim
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

-- https://github.com/nvim-lualine/lualine.nvim/wiki/Component-snippets
--- @param trunc_width number|nil trunctates component when screen width is less then trunc_width
--- @param trunc_len number|nil truncates component to trunc_len number of chars
--- @param hide_width number|nil hides component when window width is smaller then hide_width
--- @param no_ellipsis boolean whether to disable adding '...' at end after truncation
--- return function that can format the component accordingly
local function lualine_trunc(trunc_width, trunc_len, hide_width, no_ellipsis)
  return function(str)
    local win_width = vim.fn.winwidth(0)
    if hide_width and win_width < hide_width then return ''
    elseif trunc_width and trunc_len and win_width < trunc_width and #str > trunc_len then
       return str:sub(1, trunc_len) .. (no_ellipsis and '' or '...')
    end
    return str
  end
end

-- DO NOT REMOVE ANY `opts = {}`, it's NECESSARY and equivalent to require("plugin").setup({})
require('lazy').setup({
  -- Git support
  "tpope/vim-fugitive", -- Git commands in nvim
  { -- Add git related info in the signs columns and popups
    'lewis6991/gitsigns.nvim', dependencies = { 'nvim-lua/plenary.nvim' }
  },

  -- Fuzzy finder UI
  {
    'nvim-telescope/telescope.nvim',
    dependencies = {
      'nvim-lua/plenary.nvim',
    },
    opts = {
      -- defaults = {
      --   mappings = {
      --     i = {
      --       ['<C-u>'] = false,
      --       ['<C-d>'] = false,
      --     },
      --   },
      -- },
      extensions = {
        ["ui-select"] = {}
      },
    },
    init = function()
      require("telescope").load_extension("fzf")
      require("telescope").load_extension("ui-select")
    end,
  },
  { 'nvim-telescope/telescope-fzf-native.nvim', build = 'make' },
  "nvim-telescope/telescope-ui-select.nvim", -- replace nvim's option selection UI with telescope

  -- Better syntax highlighting and more syntax-based text objects
  { "nvim-treesitter/nvim-treesitter", build = ":TSUpdate" },
  "nvim-treesitter/nvim-treesitter-textobjects",

  -- Autocomplete and LSP
  "neovim/nvim-lspconfig", -- Collection of configurations for built-in LSP client
  "hrsh7th/nvim-cmp", -- Autocompletion plugin
  "hrsh7th/cmp-nvim-lsp",

  -- Snippets
  { "L3MON4D3/LuaSnip", version = "~2" }, -- Snippets plugin
  --"rafamadriz/friendly-snippets", -- Snippets for luasnip
  "saadparwaiz1/cmp_luasnip",

  -- Visual
  "sainnhe/everforest", -- Nice calm color theme
  { -- Fancier statusline
    "nvim-lualine/lualine.nvim",
    dependencies = {
      "arkav/lualine-lsp-progress",
    },
    opts = {
      options = {
        theme = "everforest",
        -- icons_enabled = false,
        -- component_separators = '|',
        -- section_separators = '',
      },
      sections = {
        lualine_a = {'mode'},
        lualine_b = {'branch', 'diff', { 'diagnostics', fmt = lualine_trunc(nil, nil, 90, true) }},
        lualine_c = {'filename'},
        lualine_x = {
          {
            'encoding',
            fmt = lualine_trunc(nil, nil, 90, true),
          },
          {
            'fileformat',
            fmt = lualine_trunc(nil, nil, 90, true),
          },
          'filetype',
          { -- Lsp server name
            function()
              local msg = 'no_lsp'
              local buf_ft = vim.api.nvim_get_option_value('filetype', { scope = "local" })
              local clients = vim.lsp.get_clients()
              if next(clients) == nil then
                return msg
              end
              for _, client in ipairs(clients) do
                local filetypes = client.config.filetypes
                if filetypes and vim.fn.index(filetypes, buf_ft) ~= -1 then
                  return client.name
                end
              end
              return msg
            end,
            icon = ' ',
            fmt = lualine_trunc(nil, nil, 90, true),
          },
          {
            'lsp_progress',
            display_components = { 'spinner', { 'title', 'percentage', 'message' } },
            spinner_symbols = { '🌑 ', '🌒 ', '🌓 ', '🌔 ', '🌕 ', '🌖 ', '🌗 ', '🌘 ' },
          },
        },
        lualine_y = {'progress'},
        lualine_z = {'location'}
      },
    },
  },
  { -- Add indentation guides even on blank lines
    "lukas-reineke/indent-blankline.nvim",
    version = "~2", -- TODO: Update to v3
    opts = {
      char = '┊',
      show_trailing_blankline_indent = false,
    },
  },
  { -- Rainbow Braces!!!
    "https://gitlab.com/HiPhish/rainbow-delimiters.nvim",
    config = function()
      local rainbow_delimiters = require("rainbow-delimiters")
      require("rainbow-delimiters.setup").setup {
        -- Highlight the entire buffer all at once
        strategy = {
          [''] = rainbow_delimiters.strategy['global'],
        },
        -- Which query to use for finding delimiters
        query = {
          [''] = 'rainbow-delimiters',
        },
        highlight = {
          'RainbowDelimiterViolet',
          'RainbowDelimiterBlue',
          'RainbowDelimiterGreen',
          'RainbowDelimiterYellow',
          'RainbowDelimiterOrange',
          'RainbowDelimiterRed',
          'RainbowDelimiterCyan',
        },
      }
    end
  },

  -- General Improvements
  { "numToStr/Comment.nvim", opts = {} }, -- "gc" to comment visual regions/lines
  "tpope/vim-sleuth", -- Auto setup shiftwidth and tabstop
  "andymass/vim-matchup", -- Better %
  { -- Easier {}, (), "", etc
    "windwp/nvim-autopairs",
    event = "InsertEnter",
    config = true,
    opts = {
      enable_moveright = false, -- TODO: see if this is really better
    }
  },
  {
    "kylechui/nvim-surround",
    version = "*", -- Use for stability; omit to use `main` branch for the latest features
    event = "VeryLazy",
    opts = {},
  },
  { -- session manager
    "rmagatti/auto-session",
    dependencies = { "nvim-lua/plenary.nvim" },
    init = function()
      vim.o.sessionoptions="blank,buffers,curdir,folds,help,tabpages,winsize,winpos,terminal,localoptions"
    end,
    opts = {
      log_level = "error",
      auto_session_enabled = false,
      auto_session_create_enabled = false,
    }
  },

  -- Side panes
  { -- What if you made vim handle alternate timelines?
    "mbbill/undotree",
    keys = {
      {"<F5>", "<cmd>UndotreeToggle<cr>", desc = "UndoTree"}
    },
  },
  { -- File Manager
    "nvim-neo-tree/neo-tree.nvim",
    branch = "v3.x",
    dependencies = {
      "nvim-lua/plenary.nvim",
      "nvim-tree/nvim-web-devicons", -- not strictly required, but recommended
      "MunifTanjim/nui.nvim",
    },
    config = function(opts)
      require("neo-tree").setup(opts)
      vim.keymap.set('n', "<C-n>", "<cmd>Neotree toggle<cr>")
    end,
    opts = {
      filesystem = {
        hijack_netrw_behavior = "open_current",
      },
    },
  },

  -- Filetype support
  { "lifepillar/pgsql.vim", ft = "pgsql" }, -- Postgres support
  { "luizribeiro/vim-cooklang", ft = "cook" }, -- Cooklang support
  { "vkhitrin/vim-tera" }, -- Tera support
  { "https://tildegit.org/sloum/gemini-vim-syntax.git" }, -- Gemtext support

  -- Scrollbars
  -- { "lewis6991/satellite.nvim", opts = {} },

  --{ -- Consitent window borders
  --  'mikesmithgh/borderline.nvim',
  --  enabled = true,
  --  lazy = true,
  --  event = 'VeryLazy',
  --  opts = {},
  --},

  { -- list of keybinds
    "folke/which-key.nvim",
    event = "VeryLazy",
    init = function ()
      vim.o.timeout = true
      vim.o.timeoutlen = 500
    end,
    opts = {}
  },
})

--Set highlight on search
vim.o.hlsearch = false

--Make line numbers default
vim.wo.number = true

-- Enable relative line numbers
vim.wo.relativenumber = true

--Enable mouse mode
vim.o.mouse = 'a'

--Enable break indent
vim.o.breakindent = true

--Case insensitive searching UNLESS /C or capital in search
vim.o.ignorecase = true
vim.o.smartcase = true

--Decrease update time
vim.o.updatetime = 250
vim.wo.signcolumn = 'yes'

--Set colorscheme
vim.o.termguicolors = true
vim.o.background = 'light'
vim.g.everforest_background = 'hard'
vim.g.everforest_dim_inactive_windows = 1
vim.cmd [[colorscheme everforest]]

-- Set completeopt to have a better completion experience
vim.o.completeopt = 'menuone,noselect'

-- Add a column for showing folds
vim.o.foldcolumn = 'auto:4'

-- Save undo history
vim.o.undofile = true
vim.o.undolevels = 2137

-- Show certain invisible characters
vim.opt.list = true
vim.opt.listchars = {
  tab = '▹▹',
  -- Trailing spaces
  trail = '•',
  -- The character to show in the last column when wrap is off and the line
  -- continues beyond the right of the screen
  extends = '>',
  -- The character to show in the last column when wrap is off and the line
  -- continues beyond the right of the screen
  precedes = '<',
}

-- Fix stuff in linux terminal
if (vim.env.TERM == "linux") then
  vim.cmd [[colorscheme default]]
  vim.opt.listchars = {
    tab = '>-',
    trail = '+',
    extends = '>',
    precedes = '<',
  }
end

-- Tabs are [up to] x spaces wide
-- 4 is sane for programmers that use tabs but
-- 8 is good for file formats that use tabs for tabular data
vim.o.tabstop = 8

-- Remap space as leader key
vim.keymap.set({ 'n', 'v' }, '<Space>', '<Nop>', { silent = true })
vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

-- Highlight on yank
local highlight_group = vim.api.nvim_create_augroup('YankHighlight', { clear = true })
vim.api.nvim_create_autocmd('TextYankPost', {
  callback = function()
    vim.highlight.on_yank()
  end,
  group = highlight_group,
  pattern = '*',
})

-- TODO: add highlight on substitute, disable highlight on search

-- Gitsigns
require('gitsigns').setup {
  signs = {
    add = { text = '+' },
    change = { text = '~' },
    delete = { text = '_' },
    topdelete = { text = '‾' },
    changedelete = { text = '~' },
  },
}

--Add leader shortcuts
vim.keymap.set('n', '<leader><space>', require('telescope.builtin').buffers, { desc = "Search Open Filenames" })
vim.keymap.set('n', '<leader>sf', function()
  require('telescope.builtin').find_files { previewer = false }
end, { desc = "Search Filenames" })
vim.keymap.set('n', '<leader>sb', require('telescope.builtin').current_buffer_fuzzy_find, { desc = "Search in Buffer" })
vim.keymap.set('n', '<leader>sh', require('telescope.builtin').help_tags, { desc = "Search Help Tags" })
vim.keymap.set('n', '<leader>sd', require('telescope.builtin').grep_string, { desc = "Find Selected String in Files" })
vim.keymap.set('n', '<leader>sp', require('telescope.builtin').live_grep, { desc = "Search in Files" })
vim.keymap.set('n', '<leader>?', require('telescope.builtin').oldfiles, { desc = "Search Recent Filenames" })

-- Treesitter configuration
-- Parsers must be installed manually via :TSInstall
---@diagnostic disable-next-line: missing-fields
require('nvim-treesitter.configs').setup {
  ensure_installed = { "lua", "bash" }, -- Ensures that these are installed
  auto_install = true,  -- Automatically instals parser when you enter a buffer
  highlight = {
    enable = true, -- false will disable the whole extension
  },
  incremental_selection = {
    enable = true,
    keymaps = {
      -- descriptions for these are automatically added
      init_selection = false, -- this works in normal mode, others work in visual mode
      node_incremental = '<A-o>',
      scope_incremental = false, -- scope_incremental = 'grc',
      node_decremental = '<A-i>',
    },
  },
  indent = {
    enable = true,
  },
  textobjects = {
    swap = {
      enable = true,
      swap_next = {
        ["<leader>a"] = "@parameter.inner",
      },
      swap_previous = {
        ["<leader>A"] = "@parameter.inner",
      },
    },
    select = {
      enable = true,
      lookahead = true, -- Automatically jump forward to textobj, similar to targets.vim
      keymaps = {
        -- You can use the capture groups defined in textobjects.scm
        ['af'] = '@function.outer',
        ['if'] = '@function.inner',
        ['ac'] = '@class.outer',
        ['ic'] = '@class.inner',
      },
    },
    move = {
      enable = true,
      set_jumps = true, -- whether to set jumps in the jumplist
      goto_next_start = {
        [']m'] = '@function.outer',
        [']]'] = '@class.outer',
      },
      goto_next_end = {
        [']M'] = '@function.outer',
        [']['] = '@class.outer',
      },
      goto_previous_start = {
        ['[m'] = '@function.outer',
        ['[['] = '@class.outer',
      },
      goto_previous_end = {
        ['[M'] = '@function.outer',
        ['[]'] = '@class.outer',
      },
    },
  },
  matchup = {
    enable = true,
    disable = {},
  },
}

-- Diagnostic keymaps
vim.keymap.set('n', '<leader>e', vim.diagnostic.open_float, { desc = "Show diagnostic" })
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, { desc = "Previous diagnostic" })
vim.keymap.set('n', ']d', vim.diagnostic.goto_next, { desc = "Next diagnostic" })
--vim.keymap.set('n', '<leader>q', vim.diagnostic.setloclist, { desc = "Add Diagnostics to LocList" })

-- LSP settings
local lspconfig = require 'lspconfig'
local on_attach = function(client, bufnr)
  local opts = { buffer = bufnr }
  vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, vim.tbl_extend("force", opts, { desc = "Go to declaration" }))
  vim.keymap.set('n', 'gd', vim.lsp.buf.definition, vim.tbl_extend("force", opts, { desc = "Go to definition" }))
  vim.keymap.set('n', 'K', vim.lsp.buf.hover, vim.tbl_extend("force", opts, { desc = "LSP Hover" }))
  vim.keymap.set('n', '<leader>k', vim.lsp.buf.hover, vim.tbl_extend("force", opts, { desc = "LSP Hover" }))
  vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, vim.tbl_extend("force", opts, { desc = "Go to implementations" }))
  vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, vim.tbl_extend("force", opts, { desc = "LSP Signature Help" }))
  -- vim.keymap.set('n', '<leader>wa', vim.lsp.buf.add_workspace_folder, vim.tbl_extend("force", opts, { desc = "Add Workspace Folder" }))
  -- vim.keymap.set('n', '<leader>wr', vim.lsp.buf.remove_workspace_folder, vim.tbl_extend("force", opts, { desc = "Remove Workspace Folder" }))
  -- vim.keymap.set('n', '<leader>wl', function()
  --   vim.inspect(vim.lsp.buf.list_workspace_folders())
  -- end, vim.tbl_extend("force", opts, { desc = "List Workspace Folders" }))
  vim.keymap.set('n', '<leader>D', vim.lsp.buf.type_definition, vim.tbl_extend("force", opts, { desc = "Go to type definition" }))
  vim.keymap.set('n', '<leader>r', vim.lsp.buf.rename, vim.tbl_extend("force", opts, { desc = "Rename Symbol" })) -- previously <leader>rn
  vim.keymap.set('n', 'gr', vim.lsp.buf.references, vim.tbl_extend("force", opts, { desc = "Go to references" }))
  vim.keymap.set('n', '<leader>a', vim.lsp.buf.code_action, vim.tbl_extend("force", opts, { desc = "Code Actions" })) -- previously <leader>ca
  vim.keymap.set('n', '<leader>so', require('telescope.builtin').lsp_document_symbols, vim.tbl_extend("force", opts, { desc = "Search Symbols" }))
  vim.api.nvim_create_user_command("Format", function() vim.lsp.buf.format({ bufnr = bufnr, id = client.id }) end, {})
  vim.keymap.set('n', '<leader>F', ":Format<CR>", vim.tbl_extend("force", opts, { desc = "Format Buffer", silent = true }))
end

-- nvim-cmp supports additional completion capabilities
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require('cmp_nvim_lsp').default_capabilities(capabilities)

-- Enable the following language servers
local servers = { 'clangd', 'tsserver', 'r_language_server', 'arduino_language_server', 'gopls', 'hls' }
for _, lsp in ipairs(servers) do
  lspconfig[lsp].setup {
    on_attach = on_attach,
    capabilities = capabilities,
  }
end

lspconfig.rust_analyzer.setup {
  on_attach = function (_, bufnr)
    vim.cmd [[autocmd BufWritePre <buffer> lua vim.lsp.buf.format()]]
    on_attach(_, bufnr)
  end,
  capabilities = capabilities,
  settings = {
    ["rust-analyzer"] = {
      checkOnSave = {
        command = "check",
      }
    }
  }
}

-- https://old.reddit.com/r/neovim/comments/wls43h/pyright_lsp_configuration_for_python_poetry/
lspconfig.pyright.setup {
  on_attach = on_attach,
  capabilities = capabilities,
  on_new_config = function(config, root_dir)
    local env = vim.trim(vim.fn.system('cd "' .. root_dir .. '"; poetry env info -p 2>/dev/null'))
    if string.len(env) > 0 then
      config.settings.python.pythonPath = env .. '/bin/python'
    end
  end
}

-- Example custom server
-- Make runtime files discoverable to the server
local runtime_path = vim.split(package.path, ';')
table.insert(runtime_path, 'lua/?.lua')
table.insert(runtime_path, 'lua/?/init.lua')

lspconfig.lua_ls.setup {
  on_attach = on_attach,
  capabilities = capabilities,
  settings = {
    Lua = {
      runtime = {
        -- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
        version = 'LuaJIT',
        -- Setup your lua path
        path = runtime_path,
      },
      diagnostics = {
        -- Get the language server to recognize the `vim` global
        globals = { 'vim' },
      },
      workspace = {
        -- Make the server aware of Neovim runtime files
        library = vim.api.nvim_get_runtime_file('', true),
        -- Make the server stop suggesting "configure environment as luassert"
        -- https://github.com/LuaLS/lua-language-server/discussions/1688
        checkThirdParty = false,
      },
    },
  },
}

-- luasnip setup
local luasnip = require("luasnip")
require("luasnip.loaders.from_vscode").lazy_load()

-- nvim-cmp setup
local cmp = require 'cmp'
cmp.setup {
  snippet = {
    expand = function(args)
      luasnip.lsp_expand(args.body)
    end,
  },
  mapping = cmp.mapping.preset.insert({
    ['<C-d>'] = cmp.mapping.scroll_docs(-4),
    ['<C-u>'] = cmp.mapping.scroll_docs(4),
    ['<C-Space>'] = cmp.mapping.complete(),
    ['<CR>'] = cmp.mapping.confirm {
      behavior = cmp.ConfirmBehavior.Replace,
      select = true,
    },
    ['<Tab>'] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_next_item()
      elseif luasnip.expand_or_jumpable() then
        luasnip.expand_or_jump()
      else
        fallback()
      end
    end, { 'i', 's' }),
    ['<S-Tab>'] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_prev_item()
      elseif luasnip.jumpable(-1) then
        luasnip.jump(-1)
      else
        fallback()
      end
    end, { 'i', 's' }),
  }),
  sources = {
    { name = 'nvim_lsp' },
    { name = 'luasnip' },
  },
}

-- setup nvim-autopairs
local cmp_autopairs = require('nvim-autopairs.completion.cmp')
cmp.event:on(
  'confirm_done',
  cmp_autopairs.on_confirm_done()
)

-- Set up LSP borders
-- https://vi.stackexchange.com/questions/39074/user-borders-around-lsp-floating-windows
--local _border = "rounded"
local _border = {
  {'╭', "NormalFloat"},
  {'─', "NormalFloat"},
  {'╮', "NormalFloat"},
  {'│', "NormalFloat"},
  {'╯', "NormalFloat"},
  {'─', "NormalFloat"},
  {'╰', "NormalFloat"},
  {'│', "NormalFloat"},
}
vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(
  vim.lsp.handlers.hover, {
    border = _border
  }
)
vim.lsp.handlers["textDocument/signatureHelp"] = vim.lsp.with(
  vim.lsp.handlers.signature_help, {
    border = _border
  }
)
vim.diagnostic.config{
  float={ border = _border }
}
require('lspconfig.ui.windows').default_options = {
  border = _border
}

-- Add move line shortcuts
vim.keymap.set('n', '<A-j>', ':m .+1<CR>==', { noremap = true, desc = "Move Line Down" })
vim.keymap.set('n', '<A-k>', ':m .-2<CR>==', { noremap = true, desc = "Move Line Up" })
--vim.keymap.set('i', '<A-j>', '<Esc>:m .+1<CR>==gi', { noremap = true})
--vim.keymap.set('i', '<A-k>', '<Esc>:m .-2<CR>==gi', { noremap = true})

-- Autosession keybinds
vim.keymap.set('n', "<leader>SO", "<cmd>Autosession search<cr>", { desc = "AutoSession Search" })
vim.keymap.set('n', "<leader>SD", "<cmd>Autosession delete<cr>", { desc = "AutoSession Delete" })
vim.keymap.set('n', "<leader>So", "<cmd>SessionRestore<cr>", { desc = "AutoSession Restore" })
vim.keymap.set('n', "<leader>Sd", "<cmd>SessionDelete<cr>", { desc = "AutoSession Delete" })
vim.keymap.set('n', "<leader>Sw", "<cmd>SessionSave<cr>", { desc = "AutoSession Save" })

-- Inspired by helix
vim.keymap.set({'n', 'v'}, "<leader>y", "\"+y", { desc = "Copy to System Clipboard" })
vim.keymap.set({'n', 'v'}, "<leader>p", "\"+p", { desc = "Paste from System Clipboard" })
vim.keymap.set('n', "<leader>w", "<C-w>", { desc = "+window" })

-- TODO: matchup stuff? replacing quotes and such?

-- vim: ts=2 sts=2 sw=2 et
